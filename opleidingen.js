window.onload=function () {
    const elements = document.getElementsByClassName("opleiding");
    for (let i = 0; i < elements.length; i++) {
        const element = elements[i];

        element.addEventListener("click", () =>
            window.open(element.attributes.linkToOpleiding.value))
    }
};